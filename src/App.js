import React, { Component } from 'react';

import Notes from './containers/Notes/Notes';
import Menu from './containers/Header/Header';
import { Route } from 'react-router-dom';
import NotesTweaks from './containers/Notes/NotesTweaks/NotesTweaks';
import { IntlProvider, addLocaleData } from 'react-intl';
import cs from './MultiLang/cs.json';
import csLocaleData from "react-intl/locale-data/cs";

addLocaleData(csLocaleData);

const msg = {
  cs
};

class App extends Component {
  state = {
    language: 'en'
  };

  changeLanguageHandler = language => {
    this.setState({
      language
    });
  };

  render() {
    return (
      <IntlProvider locale={this.state.language} messages={msg[this.state.language]}>
        <React.Fragment>
          <header>
            <Menu
              lang={this.state.language}
              changeLanguageHandler={this.changeLanguageHandler}
            />
          </header>
          <Route path='/' exact component={Notes} />
          <Route path='/new' exact component={NotesTweaks} />
          <Route path={'/edit/:id'} exact component={NotesTweaks} />
        </React.Fragment>
      </IntlProvider>
    );
  }
}

export default App;
