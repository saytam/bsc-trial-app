import { getNotes } from '../RestApi/RestApi';

describe('Testing end to end connections', () => {
  it('Test getNotes method.', () => {
    return getNotes().then(response => {
      expect(Array.isArray(response.data)).toBe(true);
    });
  });
});
