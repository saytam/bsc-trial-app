# BSC Notes App

This project-app is created to manage notes with different action buttons like new, edit or delete. 

## Database

The App gets its data from rest API server localized on 'http://private-9aad-note10.apiary-mock.com/' domain. 

## Start application

What you need to run the app:

```
node.js,
end either npm or yarn installed,

npm: https://www.npmjs.com/get-npm

yarn: https://yarnpkg.com/lang/en/docs/install
```

### Installing

First you need to install 'node_modules' etc., which are currently not stored on the gitLab using .gitignore.

```
npm install
```

And then run the app

```
npm start
```

You should end with completely loaded app displaying the home page ('/') and seeing two loaded notes from the database.

## Tests

To run the End to End test type 'npm run test' into the terminal

```
npm run test
```
This test should be testing the server response to function getNotes


## Multi-lang

App is running two verses of language display, either english or czech